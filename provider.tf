provider "google" {
  credentials = file("./credentials/serviceaccount.json")
  project     = "eleanor-270008"
  region      = "europe-west1"
}