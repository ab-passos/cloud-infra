package main

import (
	"cloud.google.com/go/pubsub"
	"encoding/json"
	"context"
	"fmt"
	"log"
  )

func test() {
	ctx := context.Background()
  client, err := pubsub.NewClient(ctx, "eleanor-270008")
  if err != nil {
    // TODO: Handle error.
    fmt.Println(err)
  } else {
    fmt.Println(client)
  }

  fmt.Println("OK2")

  topic := client.Topic("FileAdded")
  ok, err := topic.Exists(ctx)
  if err != nil {
    fmt.Println(err)
    // TODO: Handle error.
  }

  if !ok {
    fmt.Println("Not ok")
    // Topic doesn't exist.
  } else {
    fmt.Println("Exists")
  }
}

func listen() {
	ctx := context.Background()
	client, err := pubsub.NewClient(ctx, "eleanor-270008")
	if err != nil {
		log.Fatal(err)
	}
	sub := client.Subscription("FileAddedSubscription")

	err2 := sub.Receive(context.Background(), 
		func(ctx context.Context, m *pubsub.Message) {
			log.Printf("Got message: %s", m.Data)
			var raw map[string]interface{}
			if err := json.Unmarshal(m.Data, &raw); err != nil {
				panic(err)
			}

			out, _ := json.Marshal(raw["selfLink"])
			println(string(out))
			m.Ack()
		})
	if err2 != nil {
	   // Handle error.
	}
}

func main() {
	listen()
}