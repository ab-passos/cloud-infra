resource "google_storage_bucket" "patch-store" {
  name     = "patch-store-bucket"
  location = "EU"
  force_destroy=true
}

resource "google_storage_bucket" "vm-tooling-store" {
  name     = "vm-tooling"
  location = "EU"
  force_destroy=true
}