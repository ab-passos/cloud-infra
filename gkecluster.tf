resource "google_container_cluster" "gke-cluster" {
  name               = "hello-cluster"
  network            = "default"
  location           = "us-west1"
  initial_node_count = 1
}