resource "google_pubsub_topic" "TopicFileAdded" {
  name = "FileAdded"
}
resource "google_storage_notification" "notification" {
  bucket         = "patch-store-bucket"
  payload_format = "JSON_API_V1"
  topic          = "FileAdded"
  event_types    = ["OBJECT_FINALIZE"]
  custom_attributes = {
    new-attribute = "new-attribute-value"
  }

  depends_on = [
    google_pubsub_topic.TopicFileAdded,
    google_pubsub_topic_iam_binding.binding,
    google_storage_bucket.patch-store
  ]
}

resource "google_pubsub_topic" "vmNotification" {
  name = "vm-notification"
}

data "google_storage_project_service_account" "gcs_account" {
}

resource "google_pubsub_topic_iam_binding" "binding" {
  topic   = google_pubsub_topic.TopicFileAdded.id
  role    = "roles/pubsub.publisher"
  members = ["serviceAccount:${data.google_storage_project_service_account.gcs_account.email_address}"]
}

resource "google_pubsub_subscription" "FileAddedSubscription" {
  name  = "FileAddedSubscription"
  topic = google_pubsub_topic.TopicFileAdded.id

  ack_deadline_seconds = 20
}

resource "google_pubsub_subscription" "messagesVm" {
  name  = "messages-vm"
  topic = google_pubsub_topic.vmNotification.id

  ack_deadline_seconds = 20
}